topic_group,id,title,goal
Group_8,5a4edd27c6,Alibaba's FashionAI shows how machine learning might save the mall,To make predictive guesses to help people using the AI get recommendations for what clothes the user is interested in and help make fashion tips for the user.
Group_8,f9aa29224b,An AI has learned how to pick a single voice out of a crowd | New Scientist,An AI using machine learning to separate the voices of multiple speakers in real time.
Group_8,7bd4003b9b,Sophia Genetics is machine learning is speeding up cancer diagnosis | WIRED UK,"The goal is to detect cancer in the human body as well as congenital diseases, by sequencing the genomes of patient's tissue samples and then use machine learning to suggest the most effective treatments."
Group_8,9d69e0e1ab,Machine Learning Is Aiding in the Fight Against Mental Illness,To develop a machine learning algorithm trained to understand neural representations of suicidal behaviour.
Group_8,c1d36f1622,Microsoft’s machine learning can predict injuries in sports,To help teams track and improve player behavior in sports.
