import os
import re
import csv
import re
import pandas as pd
import urllib.request
from bs4 import BeautifulSoup
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.decomposition import LatentDirichletAllocation, NMF

def text_file_handler(path):
    """Returns list of blurb dictionaries for given path.

    This function takes an input string path and for every text file
    in the passsed directory path it attempts to parse the information in the
    file into a python dictionary.
    Args:
        path (str): Path to blurb directory.


    Returns:
        list: List of parsed blurb dictionaries.
    """
    # Empty Blurb List
    blurbs = []

    # Loop through all files in blurb directory
    for file in os.listdir(path):

        # Template for blurb dict
        current_blurb = {"FILE": file,
                         "SOURCE": "",
                         "AGENT": "",
                         "GOAL": "",
                         "DATA": "",
                         "METHODS": "",
                         "RESULTS": "",
                         "COMMENTS": ""}

        # Full path to current blurb file
        file_path = os.path.join(path, file)

        # Open blurb file in read mode
        with open(file_path, 'r', encoding="ISO-8859-1") as myfile:

            # Replace newline characters with spaces. Helps the parser
            file_text = myfile.read().replace('\n', ' ')

            # Parse relevant information into correct dictionary keys
            current_blurb["SOURCE"] = find_between(file_text,
                                                   "SOURCE",
                                                   "AGENT").strip()
            current_blurb["AGENT"] = find_between(file_text,
                                                  "AGENT",
                                                  "GOAL").strip()
            current_blurb["GOAL"] = find_between(file_text,
                                                 "GOAL",
                                                 "DATA").strip()
            current_blurb["DATA"] = find_between(file_text,
                                                 "DATA",
                                                 "METHODS").strip()
            current_blurb["METHODS"] = find_between(file_text,
                                                    "METHODS",
                                                    "RESULTS").strip()
            current_blurb["RESULTS"] = find_between(file_text,
                                                    "RESULTS",
                                                    "COMMENTS").strip()
            current_blurb["COMMENTS"] = find_after(file_text,
                                                   "COMMENTS").strip()

            blurbs.append(current_blurb)

    # Return list of blurb dictionaries
    return blurbs


def find_between(text, first, last):
    """Returns text bewteen two supplied key words in a body of text.
    Args:
        text (str): Body of text to search.
        first (str): Start word to search from.
        last (str): End word to search too.
    Returns:
        str: Body of text between suppiled start and end words.
    """
    try:
        start = text.index(first) + len(first)
        end = text.index(last, start)
        return text[start:end]
    except ValueError:
        return ""
    except IndexError:
        try:
            return text.split(first, 1)[1]
        except ValueError:
            return ""


def find_after(text, first):
    """Returns text after supplied key word in a body of text. Needed when
    blurb is badly formated or missing header
    Args:
        text (str): Body of text to search.
        first (str): Start word to search from.
    Returns:
        str: Body of text after suppiled start word.
    """
    try:
        return text.split("COMMENTS", 1)[1]
    except ValueError:
        return ""
    except IndexError:
        return ""


def get_only_text_from_url(url):
    """This function takes in a URL as an argument, and returns only the text of
    the article in that URL. If scrapping fails a none value is returned.
    Args:
        url (str): Url string.
    Returns:
        str: Body of text of webpage article."""
    try:
        page = urllib.request.urlopen(url).read().decode('utf8')
    except:
        print("None")
        return(None, None)
    # download the URL
    soup = BeautifulSoup(page, 'lxml')

    if soup is None:
        return(None, None)

    # kill all script and style elements
    for script in soup(["script", "style"]):
        script.extract()    # rip it out

    # get text
    text = ' '.join(map(lambda p: p.text, soup.find_all('p')))

    # break into lines and remove leading and trailing space on each
    lines = (line.strip() for line in text.splitlines())
    # break multi-headlines into a line each
    chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
    # drop blank lines
    text = '\n'.join(chunk for chunk in chunks if chunk)

    # Get title of news article
    text_title = ""

    # If article title is None replace with empty string.
    if soup.title is None:
        text_title = ""
    else:
        text_title = soup.title.text

    # Retrun text title and body
    return [text_title, text]


def get_urls(url_text):
    """This function takes in a text string an attempts to locate URL's
    using regular expressions
    Args:
        url_text (str): String of text possibly containg a URL.
    Returns:
        list: List of string URL's."""

    urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', url_text)
    return urls


def get_goal(id):
    # Change path to your path
    file_path = "/home/daire/Code/CS401_Projects/Machine_Learning_in_the_Public_Eye/blurbs/"
    file_path += id
    with open(file_path, 'r', encoding="ISO-8859-1") as myfile:
        file_text = myfile.read().replace('\n', ' ')
        return find_between(file_text,
                            "GOAL",
                            "DATA").strip()

def topic_grouper(path):
    """This function takes in a path to acsv and tries to group text data in
    this into similar groups
    Args:
        path (str): Path to csv.
    """

    # Read in previously generated CSV of texts and ID's
    df = pd.read_csv(path, encoding='utf-8')

    # Drop data with duplicate URL's only keeping last value of duplicates
    df_unique = df.drop_duplicates(subset='url', keep='last')

    # Initiate CountVectorizer
    count_vect = CountVectorizer(stop_words='english',
                            strip_accents="unicode",
                            max_df=0.30,
                            min_df=0.07,
                            lowercase=True,
                            max_features=500)

    # Vecotize Text Data
    X = count_vect.fit_transform(df_unique["text"].values)

    # Chose number of Topics
    nr_topics = 10

    # Initiate LDA
    lda = LatentDirichletAllocation(n_components=nr_topics,
                                    random_state=3,
                                    learning_method='batch',
                                    n_jobs=-1,
                                    max_iter=100)

    # Fit LDA
    X_topics = lda.fit_transform(X)

    # Set top number of words to print
    n_top_words = 5
    count = 0

    # Get features from CountVectorizer
    feature_names = count_vect.get_feature_names()

    # For each topic from fitted LDA
    for topic in lda.components_:
        count += 1
        combination = "Topic : " + str(count) + ","

        # Get top words N for this topic
        topic_word_indexs = topic.argsort()[:-n_top_words - 1:-1]
        # Print top N words
        for index in topic_word_indexs:
            combination += feature_names[index] + ", "
        print(combination + "\n\n")


    # For each N topics write out 5 articles associated with each to a CSV
    for i in range(1, nr_topics+1):
        topic_group = "Group_" + str(i)
        # Set CSV header row
        topic_list = [["topic_group", "id", "title", "goal"]]

        topic_nr = i-1
        print("Topic       " + str(i))
        current_topic = X_topics[:, topic_nr].argsort()[::-1]
        blah = 0
        for no, atricle_index in enumerate(current_topic[:5]):
            current_article = [topic_group,
                               df['id'][atricle_index],
                               df['title'][atricle_index],
                               get_goal(df['id'][atricle_index])]

            topic_list.append(current_article)
        topic_file_name = "topic_" + str(i) + ".txt"
        print(topic_group)
        # Open and write CSV
        with open(topic_file_name, 'w') as myfile:
            writer = csv.writer(myfile)
            writer.writerows(topic_list)


def handle_blurbs(path):
    """This function takes in a path to a dirtory of blurbs and attempts
    to write a CVS of successfull web scrapping result data
    Args:
        path (str): Path to csv.
    """
    blurbs = text_file_handler(path)

    blurbs_to_group = [["id","title","text", "url"]]
    count = 0
    for blurb in blurbs:

        urls = get_urls(blurb["SOURCE"])

        print(blurb["FILE"])

        # Use only first supplied url if any
        if len(urls) > 0:
            url = urls[0]
            textOfUrl = get_only_text_from_url(url)
            if textOfUrl[0] is not None and len(textOfUrl[1])>0:
                count += 1
                temp_array = [blurb["FILE"], textOfUrl[0], textOfUrl[1], url]
                blurbs_to_group.append(temp_array)
            else:
                print("Bad result")

        print("Next Blurb")


    with open('blurbs.csv', 'w') as myfile:
        writer = csv.writer(myfile)
        writer.writerows(blurbs_to_group)


if __name__ == "__main__":
    # Home Desktop
    path = "/home/daire/Desktop/CS401_Projects/Machine_Learning_in_the_Public_Eye/blurbs"
    # Work Laptop
    # path = "/home/daire/Code/CS401_Projects/Machine_Learning_in_the_Public_Eye/blurbs"
    handle_blurbs(path)
    topic_grouper("blurbs.csv")
